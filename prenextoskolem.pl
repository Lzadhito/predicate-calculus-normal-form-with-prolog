:-op(200, fy, ~).                   % Negation
:-op(400, xfy, (/\)).               % Conjunction (not ISO associativity which is yfx)
:-op(500, xfy, (\/)).               % Disjunction (not ISO associativity which is yfx)
:-op(600, xfy, =>).                 % Implication
:-op(700, xfy, <=>).                % Equivalence

skolem_prefix('$skolem').

:- dynamic(skolem_index/1).
skolem_index(0).

fresh_skolem_atom(Atom) :-
	skolem_prefix(Prefix),
	name(Prefix, PrefixCodes),
	retract(skolem_index(Index)),
	NewIndex is Index+1,
	assert(skolem_index(NewIndex)),
	name(Index, IndexCodes),
	append(PrefixCodes, IndexCodes, AtomCodes),
	name(Atom, AtomCodes).
	
skolem_function(Args, Function) :-
	fresh_skolem_atom(Atom),
	Function =.. [Atom|Args].
	
skolem_rewrite_variable(F, V, W, W) :-
	var(F),
	F == V,
	!.
skolem_rewrite_variable(F, _, _, F) :-
	var(F),
	% F \== V,
	!.
skolem_rewrite_variable(~A0, V, W, ~A1) :-
	!,
	skolem_rewrite_variable(A0, V, W, A1).
skolem_rewrite_variable(A0 /\ B0, V, W, A1 /\ B1) :-
	!,
	skolem_rewrite_variable(A0, V, W, A1),
	skolem_rewrite_variable(B0, V, W, B1).
skolem_rewrite_variable(A0 \/ B0, V, W, A1 \/ B1) :-
	!,
	skolem_rewrite_variable(A0, V, W, A1),
	skolem_rewrite_variable(B0, V, W, B1).
skolem_rewrite_variable(exists(X, F), V, _, exists(X, F)) :-
	X == V,
	!.
skolem_rewrite_variable(exists(X, F0), V, W, exists(X, F1)) :-
	% X \== V,
	!,
	skolem_rewrite_variable(F0, V, W, F1).
skolem_rewrite_variable(forall(X, F), V, _, forall(X, F)) :-
	X == V,
	!.
skolem_rewrite_variable(forall(X, F0), V, W, forall(X, F1)) :-
	% X \== V,
	!,
	skolem_rewrite_variable(F0, V, W, F1).
skolem_rewrite_variable(Predicate0, V, W, Predicate1) :-  % processes functions and predicates
	Predicate0 =.. [F|Args0],
	skolem_rewrite_variable_in_args(Args0, V, W, Args1),
	Predicate1 =.. [F|Args1].
	
skolem_rewrite_variable_in_args([], _, _, []) :- !.
skolem_rewrite_variable_in_args([H0|T0], V, W, [H1|T1]) :- 
	skolem_rewrite_variable(H0, V, W, H1),
	skolem_rewrite_variable_in_args(T0, V, W, T1).
	
skolem_transform(forall(X, Phi), Vars, forall(X, Psi)) :-
	!,
	skolem_transform(Phi, [X|Vars], Psi).
skolem_transform(exists(X, Phi0), Vars, Psi) :-
	!,
	reverse(Vars, ReversedVars),
	skolem_function(ReversedVars, SkolemFunction),
	skolem_rewrite_variable(Phi0, X, SkolemFunction, Phi1),
	skolem_transform(Phi1, Vars, Psi).
skolem_transform(Phi, _, Phi).
	
skolem_transform(Phi, Psi) :- skolem_transform(Phi, [], Psi).

cetak :- write('Masukan prenex '), read(Input), write('Skolem Normal Form : \n'), skolem_transform(Input, Skolem), write(Skolem).


