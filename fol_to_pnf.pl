% source:
% 1. PNF: http://barrywatson.se/cl/cl_pnf.html
% 2. SNF: http://barrywatson.se/cl/cl_skolem_form.html
% 3. CNF: http://barrywatson.se/cl/cl_cnf.html

:-op(200, fy, ~).                   % Negation
:-op(400, xfy, (/\)).               % Conjunction (not ISO associativity which is yfx)
:-op(500, xfy, (\/)).               % Disjunction (not ISO associativity which is yfx)
:-op(600, xfy, =>).                 % Implication
:-op(700, xfy, <=>).                % Equivalence

% rewrite connectives

pnf_rewrite_connectives(~A0, ~A1) :- 
	!,
	pnf_rewrite_connectives(A0, A1).
pnf_rewrite_connectives(A0 /\ B0, A1 /\ B1) :-
	!,
	pnf_rewrite_connectives(A0, A1),
	pnf_rewrite_connectives(B0, B1).
pnf_rewrite_connectives(A0 \/ B0, A1 \/ B1) :-
	!,
	pnf_rewrite_connectives(A0, A1),
	pnf_rewrite_connectives(B0, B1).
pnf_rewrite_connectives(A0 => B0, ~A1 \/ B1) :-
	!,
	pnf_rewrite_connectives(A0, A1),
	pnf_rewrite_connectives(B0, B1).
pnf_rewrite_connectives(A0 <=> B0, (~A1 \/ B1) /\ (A1 \/ ~B1)) :-
	!,
	pnf_rewrite_connectives(A0, A1),
	pnf_rewrite_connectives(B0, B1).
pnf_rewrite_connectives(forall(X, F0), forall(X, F1)) :-
	!,
 	pnf_rewrite_connectives(F0, F1).
pnf_rewrite_connectives(exists(X, F0), exists(X, F1)) :-
	!,
 	pnf_rewrite_connectives(F0, F1).
pnf_rewrite_connectives(A, A).

% push negation

pnf_push_negation(~(~A0), A1) :- 
	!,
	pnf_push_negation(A0, A1).
pnf_push_negation(~(A0 /\ B0), A1 \/ B1) :-
	!,
	pnf_push_negation(~A0, A1),
	pnf_push_negation(~B0, B1).
pnf_push_negation(~(A0 \/ B0), A1 /\ B1) :-
	!,
	pnf_push_negation(~A0, A1),
	pnf_push_negation(~B0, B1).
pnf_push_negation(A0 /\ B0, A1 /\ B1) :-
	!,
	pnf_push_negation(A0, A1),
	pnf_push_negation(B0, B1).
pnf_push_negation(A0 \/ B0, A1 \/ B1) :-
	!,
	pnf_push_negation(A0, A1),
	pnf_push_negation(B0, B1).
pnf_push_negation(~ forall(X, F0), exists(X, F1)) :-
	!,
	pnf_push_negation(~F0, F1).
pnf_push_negation(~ exists(X, F0), forall(X, F1)) :-
	!,
	pnf_push_negation(~F0, F1).
pnf_push_negation(~ true, false) :- !. 
pnf_push_negation(~ false, true) :- !.
pnf_push_negation(A, A).

% rewrite variable

pnf_rewrite_variable(F, V, W, W) :-
	var(F),
	F == V,
	!.
pnf_rewrite_variable(F, _, _, F) :-
	var(F),
	% F \== V,
	!.
pnf_rewrite_variable(~A0, V, W, ~A1) :-
	!,
	pnf_rewrite_variable(A0, V, W, A1).
pnf_rewrite_variable(A0 /\ B0, V, W, A1 /\ B1) :-
	!,
	pnf_rewrite_variable(A0, V, W, A1),
	pnf_rewrite_variable(B0, V, W, B1).
pnf_rewrite_variable(A0 \/ B0, V, W, A1 \/ B1) :-
	!,
	pnf_rewrite_variable(A0, V, W, A1),
	pnf_rewrite_variable(B0, V, W, B1).
pnf_rewrite_variable(exists(X, F), V, _, exists(X, F)) :-
	X == V,
	!.
pnf_rewrite_variable(exists(X, F0), V, W, exists(X, F1)) :-
	% X \== V,
	!,
	pnf_rewrite_variable(F0, V, W, F1).
pnf_rewrite_variable(forall(X, F), V, _, forall(X, F)) :-
	X == V,
	!.
pnf_rewrite_variable(forall(X, F0), V, W, forall(X, F1)) :-
	% X \== V,
	!,
	pnf_rewrite_variable(F0, V, W, F1).
pnf_rewrite_variable(Predicate0, V, W, Predicate1) :-
	Predicate0 =.. [F|Args0],
	pnf_rewrite_variable_in_args(Args0, V, W, Args1),
	Predicate1 =.. [F|Args1].


pnf_rewrite_variable_in_args([], _, _, []) :- !.
pnf_rewrite_variable_in_args([H0|T0], V, W, [H1|T1]) :- 
	pnf_rewrite_variable(H0, V, W, H1),
	pnf_rewrite_variable_in_args(T0, V, W, T1).

% pull quantifier

pnf_pull_quantifier(forall(X, Phi0) /\ Psi, forall(Y, Phi1 /\ Psi)) :-
	!,
	pnf_rewrite_variable(Phi0, X, Y, Phi1).
pnf_pull_quantifier(Phi /\ forall(X, Psi0), forall(Y, Phi /\ Psi1)) :-
	!,
	pnf_rewrite_variable(Psi0, X, Y, Psi1).
pnf_pull_quantifier(forall(X, Phi0) \/ Psi, forall(Y, Phi1 \/ Psi)) :-
	!,
	pnf_rewrite_variable(Phi0, X, Y, Phi1).
pnf_pull_quantifier(Phi \/ forall(X, Psi0), forall(Y, Phi \/ Psi1)) :-
	!,
	pnf_rewrite_variable(Psi0, X, Y, Psi1).
pnf_pull_quantifier(exists(X, Phi0) /\ Psi, exists(Y, Phi1 /\ Psi)) :-
	!,
	pnf_rewrite_variable(Phi0, X, Y, Phi1).
pnf_pull_quantifier(Phi /\ exists(X, Psi0), exists(Y, Phi /\ Psi1)) :-
	!,
	pnf_rewrite_variable(Psi0, X, Y, Psi1).
pnf_pull_quantifier(exists(X, Phi0) \/ Psi, exists(Y, Phi1 \/ Psi)) :-
	!,
	pnf_rewrite_variable(Phi0, X, Y, Phi1).
pnf_pull_quantifier(Phi \/ exists(X, Psi0), exists(Y, Phi \/ Psi1)) :-
	!,
	pnf_rewrite_variable(Psi0, X, Y, Psi1).
pnf_pull_quantifier(forall(X, F1), forall(X, F2)) :-
	!,
	pnf_pull_quantifier(F1, F2).
pnf_pull_quantifier(exists(X, F1), exists(X, F2)) :-
	!,
	pnf_pull_quantifier(F1, F2).
pnf_pull_quantifier(A0 /\ B, A1 /\ B) :-
	pnf_pull_quantifier(A0, A1),
	!.
pnf_pull_quantifier(A /\ B0, A /\ B1) :-
	pnf_pull_quantifier(B0, B1),
	!.
pnf_pull_quantifier(A0 /\ B, A1 /\ B) :-
	pnf_pull_quantifier(A0, A1),
	!.
pnf_pull_quantifier(A /\ B0, A /\ B1) :-
	pnf_pull_quantifier(B0, B1).

pnf_pull_quantifier(A0 \/ B, A1 \/ B) :-
	pnf_pull_quantifier(A0, A1),
	!.
pnf_pull_quantifier(A \/ B0, A \/ B1) :-
	pnf_pull_quantifier(B0, B1),
	!.
pnf_pull_quantifier(A0 \/ B, A1 \/ B) :-
	pnf_pull_quantifier(A0, A1),
	!.
pnf_pull_quantifier(A \/ B0, A \/ B1) :-
	pnf_pull_quantifier(B0, B1).

pnf_pull_all_quantifiers(F0, F2) :-
	pnf_pull_quantifier(F0, F1),
	!,
	pnf_pull_all_quantifiers(F1, F2).
pnf_pull_all_quantifiers(F, F).


% Transfor FOL to Prenex

pnf_transform(A, D) :-
	pnf_rewrite_connectives(A, B),
	pnf_push_negation(B, C),
	pnf_pull_all_quantifiers(C, D).
	