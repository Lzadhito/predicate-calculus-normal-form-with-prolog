:-op(200, fy, ~).                   % Negation
:-op(0, yfx, (/\)).
:-op(400, xfy, (/\)).               % Conjunction (not ISO associativity which is yfx)
:-op(0, yfx, (\/)).
:-op(500, xfy, (\/)).               % Disjunction (not ISO associativity which is yfx)
:-op(600, xfy, =>).                 % Implication
:-op(700, xfy, <=>).                % Equivalence


% Eliminate all implications and equivalences. 

cnf_rewrite_connectives(~A0, ~A1) :- 
	!,
	cnf_rewrite_connectives(A0, A1).
cnf_rewrite_connectives(A0 /\ B0, A1 /\ B1) :-
	!,
	cnf_rewrite_connectives(A0, A1),
	cnf_rewrite_connectives(B0, B1).
cnf_rewrite_connectives(A0 \/ B0, A1 \/ B1) :-
	!,
	cnf_rewrite_connectives(A0, A1),
	cnf_rewrite_connectives(B0, B1).
cnf_rewrite_connectives(A0 => B0, ~A1 \/ B1) :-
	!,
	cnf_rewrite_connectives(A0, A1),
	cnf_rewrite_connectives(B0, B1).
cnf_rewrite_connectives(A0 <=> B0, (~A1 \/ B1) /\ (A1 \/ ~B1)) :-
	!,
	cnf_rewrite_connectives(A0, A1),
	cnf_rewrite_connectives(B0, B1).
cnf_rewrite_connectives(A, A).



       
%Recursively push negations, apply De Morgan's law where possible, and eliminate all double negations. At this point there are no implications or equivalences, only conjunctions, disjunctions, and negations. 

cnf_push_negation(~(~A0), A1) :- 
	!,
	cnf_push_negation(A0, A1).
cnf_push_negation(~(A0 /\ B0), A1 \/ B1) :-
	!,
	cnf_push_negation(~A0, A1),
	cnf_push_negation(~B0, B1).
cnf_push_negation(~(A0 \/ B0), A1 /\ B1) :-
	!,
	cnf_push_negation(~A0, A1),
	cnf_push_negation(~B0, B1).
cnf_push_negation(A0 /\ B0, A1 /\ B1) :-
	!,
	cnf_push_negation(A0, A1),
	cnf_push_negation(B0, B1).
cnf_push_negation(A0 \/ B0, A1 \/ B1) :-
	!,
	cnf_push_negation(A0, A1),
	cnf_push_negation(B0, B1).
cnf_push_negation(~ true, false).
cnf_push_negation(~ false, true).
cnf_push_negation(A, A).


% Distribute a disjunction over conjunctions where possible. The procedure cnf_distribute/2 performs one such rewriting. The procedure cnf_distribute_loop/2 gives us as many rewritings as possible. 

cnf_distribute(A \/ (B /\ C), (A \/ B) /\ (A \/ C)) :- !.
cnf_distribute((A /\ B) \/ C, (A \/ C) /\ (B \/ C)) :- !.
cnf_distribute(A0 /\ B, (A1 /\ B)) :-
	cnf_distribute(A0, A1).
cnf_distribute(A /\ B0, (A /\ B1)) :-
	cnf_distribute(B0, B1).
cnf_distribute(A0 \/ B, (A1 \/ B)) :-
	cnf_distribute(A0, A1).
cnf_distribute(A \/ B0, (A \/ B1)) :-
	cnf_distribute(B0, B1).

cnf_distribute_loop(A, C) :-
	cnf_distribute(A, B),
	!,
	cnf_distribute_loop(B, C).
cnf_distribute_loop(A, A).



% The procedure cnf_transform/2 is the entry-point. 

cnf_transform(A, D) :-
	cnf_rewrite_connectives(A, B),
	cnf_push_negation(B, C),
	cnf_distribute_loop(C, D).